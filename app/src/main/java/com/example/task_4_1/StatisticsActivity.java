package com.example.task_4_1;

/*

1 секунда = 1000 миллисекунд
1 минута = 60 секунд
1 час = 60 минут
1 день = 24 часа
1 год = 365 дней

*/

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import com.example.task_4_1.utils.Zodiac;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.task_4_1.MainActivity.EXTRA_BIRTH_DAY_OF_MONTH;
import static com.example.task_4_1.MainActivity.EXTRA_BIRTH_MONTH;
import static com.example.task_4_1.MainActivity.EXTRA_BIRTH_YEAR;

public class StatisticsActivity extends AppCompatActivity {

    @BindView(R.id.ac_user_statistics)
    TextView ac_user_statistics;

    @BindView(R.id.ac_zodiac)
    TextView ac_zodiac;

    int birthYear;
    int birthMonth;
    int birthDayOfMonth;

//  int age;

    GregorianCalendar dateOfBirth;
    GregorianCalendar currentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        birthYear = intent.getIntExtra(EXTRA_BIRTH_YEAR, -1);
        birthMonth = intent.getIntExtra(EXTRA_BIRTH_MONTH, -1);
        birthDayOfMonth = intent.getIntExtra(EXTRA_BIRTH_DAY_OF_MONTH, -1);

        dateOfBirth = new GregorianCalendar(birthYear, birthMonth - 1, birthDayOfMonth);
        currentDate = new GregorianCalendar();

/*
        int currentYear = currentDate.get(Calendar.YEAR);
        age = currentYear - birthYear;
*/

        // time in milliseconds
        long date = currentDate.getTime().getTime() - dateOfBirth.getTime().getTime();

        int days = (int) (date / 1000 / 60 / 60 / 24);

        // leap years (366 days) must also be considered
//      int ageYears = Integer.parseInt(String.valueOf(days)) / 365;
        int ageYears = (int) ((days) / 365);

        int ageDays = days % 365;
        long ageSeconds = date / 1000;

/*
        // From API 26
        LocalDate currentDate = LocalDate.now();
        LocalDate dateOfBirth = LocalDate.of(birthYear, birthMonth, birthDayOfMonth);

        long days = ChronoUnit.DAYS.between(currentDate, dateOfBirth);
        long age = ChronoUnit.YEARS.between(currentDate, dateOfBirth);
*/

        ac_user_statistics.setText("Your age: " + ageYears + " year(s) " + ageDays + " day(s)" + " or " + ageSeconds + " seconds");
        ac_zodiac.setText("Your zodiac sign: " + Zodiac.getZodiacName(birthDayOfMonth, birthMonth) + "\n"
                + "You have lived: " + days + " days");
//      ac_zodiac.setText("Your zodiac: " + Zodiac.getZodiacName(birthDayOfMonth, birthMonth + 1));  // for CalendarView widget
    }
}