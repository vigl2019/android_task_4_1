package com.example.task_4_1;

/*

Создать приложение с двумя экранами.

На первом экране пользователь вводит свое имя и дату рождения и нажимает кнопку “продолжить”.
Затем открывается второй экран, на котором пользователь видит свою статистику по прожитому времени
(сколько ему лет, сколько дней он прожил, сколько секунд ему исполнилось в момент открытия статистики) + его знак зодиака.

*/

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import com.example.task_4_1.utils.Manager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_FIRST_NAME = "extra_first_name";

    public static final String EXTRA_BIRTH_YEAR = "extra_birth_year";
    public static final String EXTRA_BIRTH_MONTH = "extra_birth_month";
    public static final String EXTRA_BIRTH_DAY_OF_MONTH = "extra_birth_day_of_month";

    @BindView(R.id.am_first_name)
    EditText am_first_name;

    @BindView(R.id.am_birth_date_hint)
    TextView am_birth_date_hint;

    @BindView(R.id.am_birth_day)
    EditText am_birth_day;

/*
    @BindView(R.id.am_birth_date)
    CalendarView am_birth_date;
*/

    @BindView(R.id.am_birth_month)
    EditText am_birth_month;

    @BindView(R.id.am_birth_year)
    EditText am_birth_year;

    @BindView(R.id.am_continue)
    Button am_continue;

//   GregorianCalendar dateOfBirth;

    int birthYear;
    int birthMonth;
    int birthDayOfMonth;

//  String selectedDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        am_first_name.requestFocus();

//      am_continue.setEnabled(false);

/*
        am_birth_date.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {

                int errorNumber = Validator.checkBirthDate(year, month, dayOfMonth);

                if(errorNumber > 0) {
                    Toast.makeText(getApplicationContext(), Validator.getErrorText(errorNumber), Toast.LENGTH_LONG).show();
                    return;
                }
                else{

                    birthYear = year;
                    birthMonth = month;
                    birthDayOfMonth = dayOfMonth;

//              dateOfBirth = new GregorianCalendar(year, month, dayOfMonth);

//              selectedDate = new StringBuilder().append(dayOfMonth).append(" - ").append(month + 1).append(" - ").append(year).toString();
                }
            }
        });
*/

    }

    //================================================================================//

/*
    @OnTextChanged(R.id.am_first_name)
    public void onTextChanged(CharSequence firstName){

        String firstNameInput = am_first_name.getText().toString().trim();

        int errorNumber = Validator.checkTextField(firstNameInput);
        if(errorNumber > 0) {
            Toast.makeText(getApplicationContext(), Validator.getErrorText(errorNumber), Toast.LENGTH_LONG).show();
            return;
        }
        else{
            am_continue.setEnabled(true);
        }
    }
*/

    //================================================================================//

    @OnClick(R.id.am_continue)
    public void OnButtonClick() {

        if (!Manager.checkFirstNameTextField(am_first_name))
            return;

        if (!Manager.checkTextView(am_birth_day))
            return;

        if (!Manager.checkTextView(am_birth_month))
            return;

        // Year also must have four digits
        if (!Manager.checkTextView(am_birth_year))
            return;

        //==================================================//

        birthDayOfMonth = Integer.parseInt(am_birth_day.getText().toString().trim());
        birthMonth = Integer.parseInt(am_birth_month.getText().toString().trim());
        birthYear = Integer.parseInt(am_birth_year.getText().toString().trim());

        if (!Manager.checkBirthDate(birthDayOfMonth, birthMonth, birthYear, getApplicationContext()))
            return;

        Intent intent = new Intent(this, StatisticsActivity.class);
        intent.putExtra(EXTRA_FIRST_NAME, am_first_name.getText().toString().trim());
        intent.putExtra(EXTRA_BIRTH_YEAR, birthYear);
        intent.putExtra(EXTRA_BIRTH_MONTH, birthMonth);
        intent.putExtra(EXTRA_BIRTH_DAY_OF_MONTH, birthDayOfMonth);

        startActivity(intent);
    }
}
