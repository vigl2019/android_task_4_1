package com.example.task_4_1.utils;

import com.example.task_4_1.interfaces.IErrors;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.task_4_1.interfaces.IErrors.FIRST_NAME_PATTERN;

public class Validator {

    private static Pattern pattern;
    private static Matcher matcher;

    public Validator() {
    }

    //================================================================================//

/*
    public static String getResult() {
        return "";
    }
*/

/*
    public static boolean hasError(TextView textView) {
        return textView.getText().toString().isEmpty();
    }
*/

    public static String getErrorText(int errorCode) {
        return IErrors.Errors.getErrorString(errorCode);
    }

    public static int checkTextView(String text){
        if (text.trim().isEmpty())
            return 15;
        return 0;
    }

    public static int checkFirstNameTextField(String text) {
        if (text.trim().isEmpty())
            return 1;

        pattern = Pattern.compile(FIRST_NAME_PATTERN);
        matcher = pattern.matcher(text.trim());

        if (!matcher.matches())
            return 2;

        return 0;
    }

    // Days of month from 1 to 31
    // Months from 1 to 12
    // Years > 0
    public static int checkBirthDate(int dayOfMonth, int month, int year) {

//      Calendar calendar = Calendar.getInstance();

        GregorianCalendar calendar = new GregorianCalendar();

        int currentDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int currentMonth = calendar.get(Calendar.MONTH);
        int currentYear = calendar.get(Calendar.YEAR);

        if (year > currentYear)
            return 3;

        if (year == currentYear) {
            if (month > currentMonth)
                return 4;
        }

        if (month == currentMonth) {
            if (dayOfMonth > currentDayOfMonth)
                return 5;
        }

        //==================================================//

        // Check dayOfMonth
        if (dayOfMonth <= 0 || dayOfMonth > 31)
            return 6;

        // Check month
        if (month < 1 || month > 12)
            return 7;

        // Check year
        if (year < 1)
            return 8;

        // Check February
        if (month == 2) {
            if (dayOfMonth > 29)
                return 9;
            if (dayOfMonth == 29) {

                // Check leap year
                if (!calendar.isLeapYear(year))
                    return 10;
/*
                if (((year % 4) == 0 && (year % 100) != 0) || (year % 400) == 0) {
                    return 0;
                } else
                    return 10;
            }
*/

            }
        }
        return 0;
    }
}

