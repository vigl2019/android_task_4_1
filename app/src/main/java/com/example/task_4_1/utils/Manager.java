package com.example.task_4_1.utils;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

public class Manager {

    public Manager() { }

    //================================================================================//

    public static boolean checkTextView(TextView textView) {

        int errorNumber = Validator.checkTextView(textView.getText().toString().trim());

        if (errorNumber > 0) {
            textView.requestFocus();
            Toast.makeText(textView.getContext(), Validator.getErrorText(errorNumber), Toast.LENGTH_LONG).show();

            return false;
        }

        return true;
    }

    public static boolean checkFirstNameTextField(TextView textView) {

        int errorNumber = Validator.checkFirstNameTextField(textView.getText().toString().trim());

        if (errorNumber > 0) {
            textView.requestFocus();
            Toast.makeText(textView.getContext(), Validator.getErrorText(errorNumber), Toast.LENGTH_LONG).show();

            return false;
        }

        return true;
    }

    public static boolean checkBirthDate(int birthDayOfMonth, int birthMonth, int birthYear, Context context){

        // Check birth date
        int errorNumber = Validator.checkBirthDate(birthDayOfMonth, birthMonth, birthYear);

        if(errorNumber > 0) {
            Toast.makeText(context, Validator.getErrorText(errorNumber), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
}




