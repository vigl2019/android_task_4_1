﻿package com.example.task_4_1.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Zodiac {

    private static final String START_DAY = "start day";
    private static final String END_DAY = "end day";

    private static Map<String, Map<String, String>> zodiac = new LinkedHashMap<>(12);

    static {

        zodiac.put("Овен", new LinkedHashMap<String, String>(2));
        zodiac.get("Овен").put(START_DAY, "21-03");
        zodiac.get("Овен").put(END_DAY, "20-04");

        zodiac.put("Телец", new LinkedHashMap<String, String>(2));
        zodiac.get("Телец").put(START_DAY, "21-04");
        zodiac.get("Телец").put(END_DAY, "21-05");

        zodiac.put("Близнецы", new LinkedHashMap<String, String>(2));
        zodiac.get("Близнецы").put(START_DAY, "22-05");
        zodiac.get("Близнецы").put(END_DAY, "21-06");

        zodiac.put("Рак", new LinkedHashMap<String, String>(2));
        zodiac.get("Рак").put(START_DAY, "22-06");
        zodiac.get("Рак").put(END_DAY, "22-07");

        zodiac.put("Лев", new LinkedHashMap<String, String>(2));
        zodiac.get("Лев").put(START_DAY, "23-07");
        zodiac.get("Лев").put(END_DAY, "23-08");

        zodiac.put("Дева", new LinkedHashMap<String, String>(2));
        zodiac.get("Дева").put(START_DAY, "24-08");
        zodiac.get("Дева").put(END_DAY, "23-09");

        zodiac.put("Весы", new LinkedHashMap<String, String>(2));
        zodiac.get("Весы").put(START_DAY, "24-09");
        zodiac.get("Весы").put(END_DAY, "23-10");

        zodiac.put("Скорпион", new LinkedHashMap<String, String>(2));
        zodiac.get("Скорпион").put(START_DAY, "24-10");
        zodiac.get("Скорпион").put(END_DAY, "22-11");

        zodiac.put("Стрелец", new LinkedHashMap<String, String>(2));
        zodiac.get("Стрелец").put(START_DAY, "23-11");
        zodiac.get("Стрелец").put(END_DAY, "22-12");

        zodiac.put("Козерог", new LinkedHashMap<String, String>(2));
        zodiac.get("Козерог").put(START_DAY, "23-12");
        zodiac.get("Козерог").put(END_DAY, "20-01");

        zodiac.put("Водолей", new LinkedHashMap<String, String>(2));
        zodiac.get("Водолей").put(START_DAY, "21-01");
        zodiac.get("Водолей").put(END_DAY, "19-02");

        zodiac.put("Рыбы", new LinkedHashMap<String, String>(2));
        zodiac.get("Рыбы").put(START_DAY, "20-02");
        zodiac.get("Рыбы").put(END_DAY, "20-03");
    }

    public Zodiac() {
    }

    public static Map<String, Map<String, String>> getZodiac() {
        return zodiac;
    }

    //================================================================================//

    public static String getZodiacName(int day, int month) {

        String zodiacName = "";

        for (Map.Entry<String, Map<String, String>> elements : getZodiac().entrySet()) {

            Map<String, String> map = elements.getValue();

            String[] startDate = map.get(START_DAY).split("-");
            String[] endDate = map.get(END_DAY).split("-");

            if (month == Integer.parseInt(endDate[1])) {
                if (day <= Integer.parseInt(endDate[0])) {
                    zodiacName = elements.getKey();
                    break;
                }
                if (month == Integer.parseInt(startDate[1])) {
                    if (day >= Integer.parseInt(startDate[0])) {
                        zodiacName = elements.getKey();
                        break;
                    }
                }
            }
        }

        return zodiacName;
    }
}

