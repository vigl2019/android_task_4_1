package com.example.task_4_1.interfaces;

import java.util.HashMap;
import java.util.Map;

public interface IErrors {

    String FIRST_NAME_PATTERN = "^[a-zA-Z]+$";
//  String FIRST_NAME_PATTERN = "^[a-zA-Z]{2,}$";

    enum Errors {

        ENTER_YOUR_FIRST_NAME("Enter your first name!"),
        ENTER_ONLY_LETTERS("Enter only letters!"),
        YOUR_YEAR_OF_BIRTH_IS_MORE_THAN_CURRENT("Your year of birth is more than current!"),
        YOUR_MONTH_OF_BIRTH_IS_MORE_THAN_CURRENT("Your month of birth is more than current!"),
        YOUR_DAY_OF_BIRTH_IS_MORE_THAN_CURRENT("Your day of birth is more than current!"),
        DAY_OF_MONTH_ERROR("Your day of birth less than 0 or more than 31"),
        MONTH_ERROR("Your month of birth less than 1 or more than 12"),
        YEAR_ERROR("Your year of birth less than 1"),
        FEBRUARY_ERROR("Day of February more than 29"),
        LEAP_YEAR_ERROR("Don't a leap year. Number of days of February must be 28"),
        ENTER_DAY_OF_MONTH("Enter day of month!"),
        ENTER_MONTH("Enter month!"),
        ENTER_YEAR("Enter day of month!"),
        EMPTY_TEXT_FIELD("Enter data!");

        static Map<Integer, String> errorsMap = new HashMap<>();

        static {
            errorsMap.put(1, ENTER_YOUR_FIRST_NAME.getErrorMassage());
            errorsMap.put(2, ENTER_ONLY_LETTERS.getErrorMassage());
            errorsMap.put(3, YOUR_YEAR_OF_BIRTH_IS_MORE_THAN_CURRENT.getErrorMassage());
            errorsMap.put(4, YOUR_MONTH_OF_BIRTH_IS_MORE_THAN_CURRENT.getErrorMassage());
            errorsMap.put(5, YOUR_DAY_OF_BIRTH_IS_MORE_THAN_CURRENT.getErrorMassage());
            errorsMap.put(6, DAY_OF_MONTH_ERROR.getErrorMassage());
            errorsMap.put(7, MONTH_ERROR.getErrorMassage());
            errorsMap.put(8, YEAR_ERROR.getErrorMassage());
            errorsMap.put(9, FEBRUARY_ERROR.getErrorMassage());
            errorsMap.put(10, LEAP_YEAR_ERROR.getErrorMassage());
            errorsMap.put(12, ENTER_DAY_OF_MONTH.getErrorMassage());
            errorsMap.put(13, ENTER_MONTH.getErrorMassage());
            errorsMap.put(14, ENTER_YEAR.getErrorMassage());
            errorsMap.put(15, EMPTY_TEXT_FIELD.getErrorMassage());
        }

        private String errorMassage;

        Errors(String errorMassage) {
            this.errorMassage = errorMassage;
        }

        public String getErrorMassage() {
            return errorMassage;
        }

        public void setErrorMassage(String errorMassage) {
            this.errorMassage = errorMassage;
        }

        public static String getErrorString(int errorCode) {
            return errorsMap.get(errorCode);
        }
    }
}

